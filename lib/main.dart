import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Form Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const FormScreen(),
    );
  }
}

class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  final _formKey = GlobalKey<FormState>();
  String nama = "";
  String password = "";
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  final passTextController = TextEditingController();

  @override
  void initState() {
    super.initState();

    // Start listening to changes.
    passTextController.addListener(_printLatestValue);
  }

  void _printLatestValue() {
    print('Second text field: ${passTextController.text}');
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    passTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Latihan Form"),
        ),
      body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        hintText: "contoh: Susilo Bambang",
                        labelText: "Nama Lengkap",
                        icon: Icon(Icons.people),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      },
                      onChanged: (value) {
                        setState(() {
                          nama = value;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: new InputDecoration(
                        labelText: "Password",
                        icon: Icon(Icons.security),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: passTextController,
                    ),
                  ),
                  CheckboxListTile(
                    title: Text('Belajar Dasar Flutter'),
                    subtitle: Text('Dart, widget, http'),
                    value: nilaiCheckBox,
                    activeColor: Colors.deepPurpleAccent,
                    onChanged: (value) {
                      setState(() {
                        nilaiCheckBox = value!;
                      });
                    },
                  ),
                  SwitchListTile(
                    title: Text('Backend Programming'),
                    subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                    value: nilaiSwitch,
                    activeTrackColor: Colors.pink[100],
                    activeColor: Colors.red,
                    onChanged: (value) {
                      setState(() {
                        nilaiSwitch = value;
                      });
                    },
                  ),
                  Slider(
                    value: nilaiSlider,
                    min: 0,
                    max: 100,
                    onChanged: (value) {
                      setState(() {
                        nilaiSlider = value;
                      });
                    },
                  ),
                  ElevatedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor:Colors.blue
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              // Retrieve the text that the user has entered by using the
                              // TextEditingController.
                              content: Text("User name: $nama with password:  ${passTextController.text}"),
                            );
                          },
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
      ),
    );
  }
}
